//
//  main.swift
//  基本数据结构
//
//  Created by durban.zhang on 2018/5/7.
//  Copyright © 2018年 durban.zhang. All rights reserved.
//

import Foundation

print("4、基本数据结构")

var v1 = ["iOS", "Swift", "Android"]
for v in v1 {
    print("v = \(v)")
}

var v2 = [
    "iOS": "iOS App",
    "Android": "Android App",
    "Window": "Window Phone App"
]

for (k, v) in v2 {
    print("\(k) = \(v)")
}

v1.append("Window")

for v in v1 {
    print("new v = \(v)")
}

v2["Swift"] = "Swift Language"
for (k, v) in v2 {
    print("new \(k) = \(v)")
}
